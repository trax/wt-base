
#include <Wt/WLayout>
#include <Wt/WVBoxLayout>
#include <Wt/WContainerWidget>
#include <Wt/WBootstrapTheme>
#include <Wt/WStackedWidget>
#include <Wt/WNavigationBar>
#include <Wt/WLineEdit>
#include <Wt/WMenu>
#include <Wt/WText>
#include <Wt/WServer>



#include "main.hpp"
#include "auth/Login.hpp"

namespace doltix {
namespace frontend {

void Main::pathChanged(const std::string &url) {
	int index;


	auto got = routes.find (url);
	if (got != routes.end()){
		index = got->second;
	} else {
		index = 0;
	}

	menuStack->setCurrentIndex (index);
	if (index == 2){
		rightMenu->select(0);
		leftMenu->select(-1);
	} else {
		leftMenu->select(index);
		rightMenu->select(-1);
	}
}

Main::Main(const Wt::WEnvironment& env): 
	Wt::WApplication(env),
	dbBackend(WApplication::instance()->appRoot() + "doltix.db")
{
  setTitle("Doltix" __TIME__);

  menuStack  = new Wt::WStackedWidget(root()); 

  //useStyleSheet("web/css.css");
  globalLayout = new Wt::WVBoxLayout();
  root()->setLayout(globalLayout);
  setTheme(new Wt::WBootstrapTheme());

  root()->setStyleClass("row-fluid");
  
	auto navBar     = createNavigationBar(menuStack, root());

  globalLayout->addWidget(navBar,0);
  globalLayout->addWidget(menuStack,0);

  globalLayout->addStretch(1);
	

	internalPathChanged().connect(this, &Main::pathChanged);
	pathChanged(internalPath ());
}

void Main::logged(bool isLogged, const std::string &userName) {
	if (isLogged){
		rightMenu->itemAt(0)->setText(userName);
	}else{
		rightMenu->itemAt(0)->setText("login");
	}
}

Wt::WNavigationBar * Main::createNavigationBar(Wt::WStackedWidget *stack, 
																							 Wt::WContainerWidget *parent) {
  Wt::WNavigationBar * navigation = new Wt::WNavigationBar(parent);
  navigation->setTitle("doltix","/");
  navigation->setResponsive(true);

  auto notification = new Wt::WText("trax Omar Giveranud o.givernaud@gmail.com");
  notification->setStyleClass("alert alert-info");

	auto login = new Login(dbBackend);
	login->logged().connect(this, &Main::logged);
	rightMenu = new Wt::WMenu(stack);
  leftMenu = new Wt::WMenu(stack);

  leftMenu->addItem("Home", new Wt::WText ("Welcome home"))
		->setLink(Wt::WLink(Wt::WLink::InternalPath, "/"));

  rightMenu->addItem("Login", login)
		->setLink(Wt::WLink(Wt::WLink::InternalPath, "/login"));

  leftMenu->addItem("About", notification)
		->setLink(Wt::WLink(Wt::WLink::InternalPath, "/about"));
  
  navigation->addMenu(leftMenu);

	routes.insert({"/", 0});
	routes.insert({"/about", 1});
	routes.insert({"/login", 2});

  Wt::WLineEdit *edit = new Wt::WLineEdit();
  navigation->addSearch(edit, Wt::AlignRight);
	navigation->addMenu(leftMenu, Wt::AlignLeft);
	navigation->addMenu(rightMenu, Wt::AlignRight);

	// rightMenu->setInternalPathEnabled();  
	// leftMenu->setInternalPathEnabled();

	return navigation;
}


}//frontend

}//doltix


Wt::WApplication *createApplication(const Wt::WEnvironment& env) {
  return new doltix::frontend::Main(env);
}

int main(int argc, char **argv) {
	
  try {
    Wt::WServer server(argv[0]);

    server.setServerConfiguration(argc, argv, WTHTTP_CONFIGURATION);
    server.addEntryPoint(Wt::Application, createApplication);

    Session::configureAuth();
			


    if (server.start()) {
      Wt::WServer::waitForShutdown();
      server.stop();
    }
  } catch (Wt::WServer::Exception& e) {
    std::cerr << e.what() << std::endl;
  } catch (std::exception &e) {
    std::cerr << "exception: " << e.what() << std::endl;
  }

  return 0;
}
