#ifndef DOLTIX_AUTH_SESSION_H_
#define DOLTIX_AUTH_SESSION_H_

#include <vector>

#include <Wt/Auth/Login>

#include <Wt/Dbo/Session>
#include <Wt/Dbo/SqlConnection>
#include <Wt/Dbo/ptr>

#include "User.hpp"

typedef Wt::Auth::Dbo::UserDatabase<AuthInfo> UserDatabase;

class Session {

private:
	Wt::Dbo::SqlConnection &dbBackend;
  mutable Wt::Dbo::Session session;
  UserDatabase *users;
  Wt::Auth::Login login;

  Wt::Dbo::ptr<User> user() const;

public:
  static void configureAuth();

  Session(Wt::Dbo::SqlConnection &dbBackend);
  ~Session();

  Wt::Auth::AbstractUserDatabase& getUsers();
  Wt::Auth::Login& getLogin() { return login; }

  std::string userName() const;

  static const Wt::Auth::AuthService& auth();
  static const Wt::Auth::AbstractPasswordService& passwordAuth();
  static const std::vector<const Wt::Auth::OAuthService *>& oAuth();

};

#endif //SESSION_H_
