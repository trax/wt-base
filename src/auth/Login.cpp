
#include "Login.hpp"
#include <stdlib.h>

#include <iostream>

#include <Wt/WLineEdit>
#include <Wt/WPushButton>
#include <Wt/WText>
#include <Wt/WVBoxLayout>
#include <Wt/WHBoxLayout>
#include <Wt/Auth/AuthWidget>

namespace doltix {

namespace frontend {

Login::Login(Wt::Dbo::SqlConnection &dbBackend,
						 Wt::WContainerWidget *parent):  
	session (dbBackend),
	done(this) {


	session.getLogin().changed().connect(this, &Login::authEvent);

	Wt::Auth::AuthWidget *authWidget
		= new Wt::Auth::AuthWidget(Session::auth(), session.getUsers(),
															 session.getLogin(), this);

	authWidget->model()->addPasswordAuth(&Session::passwordAuth());
	authWidget->model()->addOAuth(Session::oAuth());
	authWidget->setRegistrationEnabled(true);

	authWidget->processEnvironment();
 
  setOffsets(0, Wt::Top);
  animateShow
    (Wt::WAnimation(Wt::WAnimation::SlideInFromTop
                | Wt::WAnimation::Fade, Wt::WAnimation::Linear, 250));

}

void Login::authEvent() {
	bool logged = false;
	
    if (session.getLogin().loggedIn()){
      Wt::log("notice") << "User " << session.getLogin().user().id()
                        << " logged in.";
			logged = true;
    }else{
      Wt::log("notice") << "User logged out.";
    }
		done.emit(logged, session.userName());
  }


}//web

}//doltix
