#include "Session.hpp"

#include "Wt/Auth/AuthService"
#include "Wt/Auth/HashFunction"
#include "Wt/Auth/PasswordService"
#include "Wt/Auth/PasswordStrengthValidator"
#include "Wt/Auth/PasswordVerifier"
#include "Wt/Auth/GoogleService"
#include "Wt/Auth/Dbo/AuthInfo"
#include "Wt/Auth/Dbo/UserDatabase"

#include <Wt/WApplication>
#include <Wt/WLogger>

#include <unistd.h>


using namespace Wt;
namespace dbo = Wt::Dbo;

namespace {

class UnixCryptHashFunction : public Auth::HashFunction {
  public:
    virtual std::string compute(const std::string& msg, 
				const std::string& salt) const
    {
      std::string md5Salt = "$1$" + salt;
      return crypt(msg.c_str(), md5Salt.c_str());
    }

    virtual bool verify(const std::string& msg,
			const std::string& salt,
			const std::string& hash) const
    {
      return crypt(msg.c_str(), hash.c_str()) == hash;
    }

    virtual std::string name () const {
      return "crypt";
    }
  };

  class MyOAuth : public std::vector<const Auth::OAuthService *>{
  public:
    ~MyOAuth()
			{
				for (unsigned i = 0; i < size(); ++i)
					delete (*this)[i];
			}
  };

  Auth::AuthService myAuthService;
  Auth::PasswordService myPasswordService(myAuthService);
  MyOAuth myOAuthServices;
}

void Session::configureAuth() {
  myAuthService.setAuthTokensEnabled(true, "doltixcookie");
  myAuthService.setEmailVerificationEnabled(true);
	myAuthService.setEmailVerificationRequired(true);
	
  Auth::PasswordVerifier *verifier = new Auth::PasswordVerifier();
  verifier->addHashFunction(new Auth::BCryptHashFunction(7));

  verifier->addHashFunction(new UnixCryptHashFunction());

  myPasswordService.setVerifier(verifier);
  myPasswordService.setStrengthValidator(new Auth::PasswordStrengthValidator());
  myPasswordService.setAttemptThrottlingEnabled(true);
	
  if (Auth::GoogleService::configured()){
    myOAuthServices.push_back(new Auth::GoogleService(myAuthService));
	}
}

Session::Session(Wt::Dbo::SqlConnection &dbBackend): 
	dbBackend(dbBackend) {
	
  session.setConnection(dbBackend);
  dbBackend.setProperty("show-queries", "true");

  session.mapClass<User>("user");
  session.mapClass<AuthInfo>("auth_info");
  session.mapClass<AuthInfo::AuthIdentityType>("auth_identity");
  session.mapClass<AuthInfo::AuthTokenType>("auth_token");

  users = new UserDatabase(session);

  dbo::Transaction transaction(session);
  try {
    session.createTables();
    Wt::log("info") << "Database created";
  } catch (...) {
    Wt::log("info") << "Using existing database";
  }

  transaction.commit();
}


dbo::ptr<User> Session::user() const {
  if (login.loggedIn()) {
    dbo::ptr<AuthInfo> authInfo = users->find(login.user());
    dbo::ptr<User> user = authInfo->user();

    if (!user) {
      user = session.add(new User());
      authInfo.modify()->setUser(user);
    }

    return user;
  } else
    return dbo::ptr<User>();
}

std::string Session::userName() const {
  if (login.loggedIn()) {
    return login.user().identity(Auth::Identity::LoginName).toUTF8();
	} else { 
    return std::string();
	}
}

Auth::AbstractUserDatabase& Session::getUsers() {
  return *users;
}

const Auth::AuthService& Session::auth() {
  return myAuthService;
}

const Auth::AbstractPasswordService& Session::passwordAuth() {
  return myPasswordService;
}

const std::vector<const Auth::OAuthService *>& Session::oAuth() {
  return myOAuthServices;
}


Session::~Session() {
  delete users;
}
