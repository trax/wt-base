#ifndef DOLTIX_WT_LOGIN_HPP
#define DOLTIX_WT_LOGIN_HPP


#include <Wt/WDialog>
#include <Wt/WSignal>
#include <Wt/WContainerWidget>
#include <Wt/Dbo/SqlConnection>

#include <string>
#include "Session.hpp"

namespace doltix {
namespace frontend {


class Login : public Wt::WContainerWidget {
private:
	Session session;
	Wt::Signal<bool , std::string> done;

  void connect();
  
  void validate();
	void authEvent();

public:
	Login(Wt::Dbo::SqlConnection &dbBackend,
		   Wt::WContainerWidget *parent = nullptr);
	const Session & getSession  ()const {return session;}
	Wt::Signal<bool, std::string>& logged() { return done; }

};

}//frontend
}//doltix



#endif // DOLTIX_WT_LOGIN_HPP



