#ifndef DOLTIX_MAIN_HPP
#define DOLTIX_MAIN_HPP

#include <Wt/WApplication>
#include <Wt/WEnvironment>

#include <string>
#include <unordered_map>
#include <Wt/Dbo/backend/Sqlite3>

namespace doltix {
namespace frontend {

class Main : public Wt::WApplication {
private:
	std::unordered_map<std::string, int> routes;
	Wt::Dbo::backend::Sqlite3 dbBackend;
	
  Wt::WVBoxLayout *globalLayout;
	Wt::WStackedWidget *menuStack;
	Wt::WMenu *rightMenu;
	Wt::WMenu *leftMenu;
	Wt::WNavigationBar * createNavigationBar(Wt::WStackedWidget *stack, 
																					 Wt::WContainerWidget *parent);
	void pathChanged(const std::string &);
	void logged(bool isLogged, const std::string &userName);
	
public:
	Main(const Wt::WEnvironment& env);
};

	
}//wt	
  
}//doltix


#endif /* DOLTIX_MAIN_HPP */
